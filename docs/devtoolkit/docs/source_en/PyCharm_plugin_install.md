# PyCharm Plug-in Installation

<a href="https://gitee.com/mindspore/docs/blob/master/docs/devtoolkit/docs/source_en/PyCharm_plugin_install.md" target="_blank"><img src="https://mindspore-website.obs.cn-north-4.myhuaweicloud.com/website-images/master/resource/_static/logo_source_en.png"></a>

## Installation Steps

1. Obtain [Plug-in Zip package](https://ms-release.obs.cn-north-4.myhuaweicloud.com/2.0.0rc1/IdePlugin/any/MindSpore_Dev_ToolKit-2.0.0.zip).
2. Start Pycharm and click on the upper left menu bar, select File->Settings->Plugins->Install Plugin from Disk.
   As shown in the figure:

   ![image-20211223175637989](./images/clip_image050.jpg)

3. Select the plug-in zip package.